GTK3 and Cairo
Drawing Example

Compilation:
gcc transition.c rgbmap.c GTK3-Cairo.c -lm `pkg-config --cflags --libs gtk+-3.0` -O3 -o gtk3cd

Programmatic Demonstrations:

  1) programmatically generated RGB buffers converted to cairo surface for display
  2) a permanent bg surface used as the display surface
  3) temporary surface used to display layers of other drawing info
  4) copying part of a bg surface to another bg surface
  5) sourcing multiple background surfaces for display (via the draw handler) according to a deterministic display state
  6) a pixel to user coordinate (and back) conversion system
  7) dragging with mouse-down and mouse-up, with effect
  8) mouse leaving and entering drawing area, with effect
  9) employ transition effect using alpha channel

Drawing Transition Effects

  1) multiple signals attached to single drawing area
  2) framework for adding multiple transition effects
  3) request to execute a specific transition is settable from within program:
    3.1) by specifying the length of the transition effect itself (in mSecs)
  4) one transition type of FADE defined

User functions:

  1) startup - display program-generated RGB buffer
  2) drag/mouse-UP - draw dashed lines as cross hairs at mouse point
  3) drag/mouse-DOWN - draw dashed lines - display user-defined bounding box
  4) mouse-UP/post-Drag - draw solid lines - display bounding box
  5) drawing area enter - display color RGB map, immediate display, plus all bounding boxes
  5) drawing area leave - FADE transition to GREY map, RGB map on boxed sections
  6) mouse-CLICK/no-Drag - start over
  7) resize window - start over
