#ifndef __DSAMPLE__
#define __DSAMPLE__

#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <gtk/gtk.h>
#include <math.h>

enum {
	BYW,
	BYH
};

enum {
	MOUSEIN,
	MOUSEOUT,
	TTLCSURFS
};

enum {
	TRANS_FADE,
	TTLTRANSITIONS
};

typedef struct {
	gint     startX, startY, endX, endY;
	gboolean mouseDown;
	gboolean mouseIn;
} MouseState;

typedef struct {
    int		logx, logy;
    double	user_xmin, user_xmax;
    float	user_ymin, user_ymax;
    float	user_xdif, user_ydif;
    int		pix_xmin, pix_xmax;
    int		pix_ymin, pix_ymax;
    int		pix_ydif, pix_xdif;
} GRAPH_MTRX;

#define MAX_PIXEL_VALUE 255
typedef struct {
	guchar R, G, B;
} myColorStruct;

#define LOG10(x) ((double) (log(x)*.434294))
#define POW10(x) ((double) (exp(x*2.302585)))
#define USER2PIXX(gr, x) ((rint)(((((gr)->logx?LOG10((double)x):(double)x)-(gr)->user_xmin)/(gr)->user_xdif)*((double) (gr)->pix_xdif)+((double) (gr)->pix_xmin)))
#define PIX2USERX(gr, x) ((gr)->logx?POW10((((double)(x-(gr)->pix_xmin))/((double)(gr)->pix_xdif)*(double)((gr)->user_xdif)+(gr)->user_xmin)):(((double)(x-(gr)->pix_xmin))/((double)(gr)->pix_xdif)*(double)((gr)->user_xdif)+(gr)->user_xmin))
#define USER2PIXY(gr, y) ((int)(((double)(gr)->pix_ydif)-((double)(((gr)->logy?LOG10(y):y)-(gr)->user_ymin)/(gr)->user_ydif)*((double)(gr)->pix_ydif)+((double)(gr)->pix_ymin)))
#define PIX2USERY(gr, y) ((gr)->logy?POW10(((1.-(((double)y-(gr)->pix_ymin)/((double)(gr)->pix_ydif)))*(gr)->user_ydif+(gr)->user_ymin)):((1.-(((double)y-(gr)->pix_ymin)/((double)(gr)->pix_ydif)))*(gr)->user_ydif+(gr)->user_ymin))

extern void cairo_surface_copy(cairo_surface_t **to, cairo_surface_t *from, GtkWidget *da);
extern myColorStruct * colorP(float prob);
extern void window(GRAPH_MTRX *gr, double x1, double x2, double y1, double y2);
extern void viewport(GRAPH_MTRX *gr, int x1, int x2, int y1, int y2);
extern void doTransition(GtkWidget *da, cairo_surface_t *fromSurf, cairo_surface_t *toSurf);

#endif
