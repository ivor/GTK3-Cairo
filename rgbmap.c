#include "CDSample.h"

int CG[6][3] =   // color spectrum
	{{255, 0, 255},  // R = 255->0
	 {0, 0, 255},  // G = 0->255
	 {0, 255, 255},  // B = 255->0
	 {0, 255, 0},  // R = 0->255
	 {255, 255, 0},  // G = 255->0
	 {240, 240, 240}}; // 0%

myColorStruct * colorP(float prob)
{	// probability to color conversion routine
//  return the color to be associated with the given input of probability:
//  each probability falls into a colour range as follows:
//  0 - 1%:   240,240,240 - 255,000,255(grey(background) to purple)
//  1 - 6%:   255,000,255 - 000,000,255(purple to blue)
//  6 - 12%:  000,000,255 - 000,255,255(blue to cyan)
//  12 - 18%: 000,255,255 - 000,255,000(cyan to green)
//  18 - 24%: 000,255,000 - 255,255,000(green to yellow)
//  24 - 30%: 255,255,000 - 255,000,000(yellow to red)
//
	int      probI = prob, cg;
	static myColorStruct probColor;
	
	if(prob < 1.f)
	{
		probColor.R = 240 - prob*27.;  // R = 213 - 240, 1% - 0%
		probColor.G = 240 - prob*240.;  // G =   0 - 240, 1% - 0%
		probColor.B = 240 + prob*15.;  // B = 255 - 240, 1% - 0%
		return &probColor;
	}
	
	cg = probI/6;
	if(cg>4) // normalize all probs > 30...
	{
		cg = 4;
		prob = 30.f;
	}
	
	probColor.R = CG[cg][0]; probColor.G = CG[cg][1]; probColor.B = CG[cg][2];
	
	switch(cg)
	{
		case 0:
			probColor.R = 255 -(prob*(MAX_PIXEL_VALUE/6.)); // R=255->0
		break;             // 1% - 6%
		case 1:
			probColor.G =(prob-6.)*(MAX_PIXEL_VALUE/6.);  // G=0->255
		break;             // 6% - 12%
		case 2:
			probColor.B = 255 -((prob-12.)*(MAX_PIXEL_VALUE/6.)); // B=255->0
		break;             // 12% - 18%
		case 3:
			probColor.R =(prob-18.)*(MAX_PIXEL_VALUE/6.);  // R=0->255
		break;             // 18% - 24%
		case 4:
			probColor.G = 255 -((prob-24.)*(MAX_PIXEL_VALUE/6.)); // G=255->0
		break;             // 24% - 30%
	}
	return &probColor;
}

void window(GRAPH_MTRX *gr, double x1, double x2, double y1, double y2)
{
	gr->logx = gr->logy = 0;
	gr->user_xmin = x1;
	gr->user_xmax = x2;
	gr->user_ymin = y1;
	gr->user_ymax = y2;
	gr->user_xdif = gr->user_xmax - gr->user_xmin;
	gr->user_ydif = gr->user_ymax - gr->user_ymin;
}
void viewport(GRAPH_MTRX *gr, int x1, int x2, int y1, int y2)
{
	gr->pix_xmin = x1;
	gr->pix_xmax = x2;
	gr->pix_xdif = x2 - x1;
	gr->pix_ymin = y1;
	gr->pix_ymax = y2;
	gr->pix_ydif = y2 - y1;
}
