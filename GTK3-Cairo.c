#include "CDSample.h"

// some globals
static MouseState  mouseState;
static gboolean colorDir;
static cairo_surface_t 
		*CSurf[TTLCSURFS],			// the current surface for MOUSEIN and OUT  
		*CSave[TTLCSURFS],			// saved surface of last draw
		*CSource;					// pointer to surface to draw to screen
cairo_surface_t *CTransition;		// holding our transition frame
int transFlag[TTLTRANSITIONS];		// double as: flag to transition and length (in msecs)

void my_cairo_surface_copy(cairo_surface_t **to, cairo_surface_t *from, GtkWidget *da)
{
	// copy one surface to another based on a drawing widget
	// destory *to version if it exists
	if (*to)
		cairo_surface_destroy(*to);

	*to = gdk_window_create_similar_surface(gtk_widget_get_window(da), 
				CAIRO_CONTENT_COLOR, 
				gtk_widget_get_allocated_width(da), 
				gtk_widget_get_allocated_height(da));
	cairo_t *cr = cairo_create(*to);
	cairo_set_source_surface(cr, from, 0, 0);
	cairo_paint(cr);
	cairo_destroy(cr);
}

// make our RBG Buffer and convert to pixmap for display
static void makeRGBpmap(GtkWidget *da, gboolean dir)
{
	gint w = gtk_widget_get_allocated_width(da);
	gint h = gtk_widget_get_allocated_height(da);
	gint stride = cairo_format_stride_for_width(CAIRO_FORMAT_RGB24, w);
	
	int  i, j, y, y1, lumin;
	GRAPH_MTRX grL;
	static guchar *rgbbufC, *rgbbufG;
	
	if (CSurf[MOUSEIN])
	{
		free(rgbbufC);
		free(rgbbufG);
		cairo_surface_destroy(CSurf[MOUSEIN]);
		cairo_surface_destroy(CSurf[MOUSEOUT]);
	}
	rgbbufC = malloc(stride * h);
	rgbbufG = malloc(stride * h);
	
	viewport(&grL, 0, w, 0, h);
	window(&grL,(double) 0,(double) w,(double) 0.,(double) 30.0);
	
	for(i=0;i<h;i++)
	{
		guchar *posC, *posG;
		guint32 *pixelC, *pixelG;
		guint32 offset;
		myColorStruct *colorProb;
		
		for(j=0;j<w;j++)
		{
			colorProb = (dir == BYW) 
						? colorP((float) PIX2USERY(&grL, i))
						: colorP((float) PIX2USERY(&grL, j));
			lumin =(int)(colorProb->R*.299+colorProb->G*.587+colorProb->B*.114);
//			lumin =(int)(colorProb->R*.21+colorProb->G*.72+colorProb->B*.07);
			offset = (i*stride) + j*4;
			posC = rgbbufC + offset;
			posG = rgbbufG + offset;
			pixelC = (guint32*) posC;
			*pixelC = (0 << 24) | (colorProb->R << 16) | (colorProb->G << 8) | (colorProb->B);
			pixelG = (guint32*) posG;
			*pixelG = (0 << 24) | (lumin << 16) | (lumin << 8) | (lumin);
		}
	}
	
	CSurf[MOUSEIN] = cairo_image_surface_create_for_data(rgbbufC, CAIRO_FORMAT_RGB24, w, h, stride);
	CSurf[MOUSEOUT] = cairo_image_surface_create_for_data(rgbbufG, CAIRO_FORMAT_RGB24, w, h, stride);

	CSource = CSurf[MOUSEIN];
	return;
}

static void drawBox(cairo_t *cr, MouseState *mouse)
{
	int w = mouse->endX - mouse->startX + 1;
	int h = mouse->endY - mouse->startY + 1;
	cairo_set_source_rgb(cr, 1, 1, 1);
	cairo_set_line_width(cr, 1);
	cairo_set_operator(cr, CAIRO_OPERATOR_DIFFERENCE);
	cairo_rectangle(cr, mouse->startX+.5, mouse->startY+.5, w, h);
	cairo_stroke(cr);
}

#define ORDER(a, b)	{if (a > b) { gint tmp = a; a = b, b = tmp;}}
	
static gboolean doMouse(GtkWidget *da, GdkEventButton *event, MouseState *mouse)
{	// got a cat?
	gint w = gtk_widget_get_allocated_width(da);
	gint h = gtk_widget_get_allocated_height(da);

	switch(event->type)
	{
		case GDK_BUTTON_PRESS:
			mouse->startX = event->x;
			mouse->startY = event->y;
			mouse->mouseDown = TRUE;
		break;
		case GDK_BUTTON_RELEASE:
		{
			mouse->endX = event->x;
			mouse->endY = event->y;
			gboolean reset = (mouse->startX == mouse->endX && 
								mouse->startY == mouse->endY)
								? TRUE
								: FALSE;
			ORDER(mouse->startX, mouse->endX);
			ORDER(mouse->startY, mouse->endY);

			switch (reset)
			{
				case TRUE:		// start over
					makeRGBpmap(da, colorDir);
				break;
				case FALSE:
				{
					cairo_surface_t *surface;
					cairo_t *cr;
					gint mw = mouse->endX - mouse->startX + 1;
					gint mh = mouse->endY - mouse->startY + 1;
					
					// copy box to OUT surface
					surface = CSurf[MOUSEOUT];
					cr = cairo_create(surface);
					cairo_set_source_surface(cr, CSurf[MOUSEIN], 0, 0);
					cairo_rectangle(cr, mouse->startX, mouse->startY, mw, mh);
					cairo_fill(cr);
					cairo_destroy(cr);

					// draw box on a copy of IN surface for display
					cr = cairo_create(CSurf[MOUSEIN]);
					drawBox(cr, mouse);
					cairo_destroy(cr);
					CSource = CSurf[MOUSEIN];
				}
				break;
			}
			gtk_widget_queue_draw_area(da, 0, 0, w, h);
			mouse->mouseDown = FALSE;
		}
		break;
	}
	
	return TRUE;
}

static gboolean doDrag(GtkWidget *da, GdkEventMotion *event, MouseState *mouse)
{ // as often as possible
	gint        x, y;
	gint w = gtk_widget_get_allocated_width(da);
	gint h = gtk_widget_get_allocated_height(da);
	static cairo_surface_t *surface;

	if (!surface)
		surface = cairo_image_surface_create(CAIRO_FORMAT_RGB24, w, h);
	cairo_t *cr = cairo_create(surface);
	cairo_set_source_surface(cr, CSurf[MOUSEIN], 0, 0);
	cairo_paint(cr);
    
	switch(mouse->mouseDown)
	{
		case FALSE:  // draw dashed lines as cross-hairs
		{
			const double	dash[2] = {4, 4};
			cairo_set_source_rgb(cr, 1, 1, 1);
			cairo_set_line_width(cr, 1);
			cairo_set_dash(cr, dash, 2, 0);
			cairo_set_operator(cr, CAIRO_OPERATOR_DIFFERENCE);
			
			cairo_move_to(cr, event->x-.5, 0);
			cairo_line_to(cr, event->x-.5, h);
			cairo_stroke(cr);
			cairo_move_to(cr, 0, event->y-.5);
			cairo_line_to(cr, w, event->y-.5);
			cairo_stroke(cr);
		}
		break;
		case TRUE:  // draw lines as box
			mouse->endX = event->x;
			mouse->endY = event->y;
			drawBox(cr, mouse);
		break;
	}
	cairo_destroy(cr);

	CSource = surface;
	gtk_widget_queue_draw(da);
	
	// tell mainloop we're ready to be called again...
	gdk_window_get_device_position(event->window, event->device, &x, &y, NULL); 
	return TRUE;
}

static gboolean doFocus(GtkWidget *da, GdkEvent *event, MouseState *mouse)
{
	gint w = gtk_widget_get_allocated_width(da);
	gint h = gtk_widget_get_allocated_height(da);
	switch(event->type)
	{
		case GDK_ENTER_NOTIFY:
			mouse->mouseIn = TRUE;
			CSource = CSurf[MOUSEIN];
		break;
		case GDK_LEAVE_NOTIFY:
			mouse->mouseIn = FALSE;
			CSource = CSurf[MOUSEOUT];
			transFlag[TRANS_FADE] = 600;		// flag to turn on FADE transition, lasting 650 mSecs
		break;
	}
	gtk_widget_queue_draw_area(da, 0, 0, w, h);
	return TRUE;
}

/*
 we define two 'draw' events attached as signals to the DA
 they will execute in the same order as registered by g_signal_connect()
 
 transitionME() is called first:
   - if transition flag has been set, call transition routine: 
       - start a timer to transition between the old and new plots
   - draw current transition surface (frame) to the display
   - return TRUE during a transition event, propagation of event is stopped, exposeME() is not called
   - return FALSE when no transition in effect, propagation of event continues to exposeME()
   
 exposeME() is called second:
   - only when transitionME() returns FALSE
   - draw current surface to the display 
*/

static gboolean transitionME(GtkWidget *da, cairo_t *cr, gpointer nil)
{	// transition callback for all drawing areas - draw event
	// TRUE = we've handled this, stop propagating the event
	// FALSE = we've not handled this, continue propagating the event

	static int transFlagC[TTLTRANSITIONS];		// empty array (transFlags) for quick testing

	// transition execution rules
	// 0) if mouse = IN, skip this:
	//		even if a transition is busy, we ignore its results and move to next draw handler immediately
	//		this way, the DA is updated with the proper surface as per the user's action 
	if (mouseState.mouseIn)
		return FALSE;
	
	// 1) have we been told to do a transition?
	if (!memcmp((void *)transFlag, (void *)transFlagC, sizeof(TTLTRANSITIONS*sizeof(int))))
		return FALSE;
	
	doTransition(da, CSurf[MOUSEIN], CSurf[MOUSEOUT]);
	cairo_set_source_surface(cr, CTransition, 0, 0);
	cairo_paint(cr);

	return TRUE;
}

static gboolean exposeME(GtkWidget *da, cairo_t *cr, gpointer nil)
{
	cairo_set_source_surface(cr, CSource, 0, 0);	// CSource points to surface to draw, set elsewhere
	cairo_paint(cr);

	// save what was just drawn, for use when a transition is specified
	// for this implementation, we don't need to save MOUSEIN surface since we never transition to there 
	if (!mouseState.mouseIn)
		my_cairo_surface_copy(&CSave[MOUSEOUT], CSource, da);
	return TRUE;
}

static gboolean configME(GtkWidget *da, GdkEventConfigure *event, gpointer nil)
{
	makeRGBpmap(da, colorDir);
	return TRUE;
}

static void activate(GtkApplication *app, gpointer user_data)
{
	GtkWidget    *topWindow, *frame, *da;

	topWindow = gtk_application_window_new(app);
	gtk_container_set_border_width(GTK_CONTAINER(topWindow), 2);
	
	GtkWidget *box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	frame = gtk_frame_new(NULL);
	gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
	da = gtk_drawing_area_new();
	gtk_widget_set_size_request(da, 300, 350);
	gtk_container_add(GTK_CONTAINER(frame), da);
	GtkWidget *button = gtk_button_new_with_label("QUIT");
	g_signal_connect_swapped(button, "clicked", G_CALLBACK(gtk_widget_destroy), topWindow);
	gtk_box_pack_start(GTK_BOX(box), frame, TRUE, TRUE, 1);
	gtk_box_pack_start(GTK_BOX(box), button, FALSE, FALSE, 1);
	gtk_container_add(GTK_CONTAINER(topWindow), box);
	
	g_signal_connect(da, "configure-event", G_CALLBACK(configME), NULL);
	g_signal_connect(da, "draw", G_CALLBACK(transitionME), NULL);
	g_signal_connect(da, "draw", G_CALLBACK(exposeME), NULL);
	g_signal_connect(da, "button-press-event", G_CALLBACK(doMouse),	(gpointer) &mouseState);
	g_signal_connect(da, "button-release-event", G_CALLBACK(doMouse), (gpointer) &mouseState);
	g_signal_connect(da, "motion-notify-event", G_CALLBACK(doDrag),	(gpointer) &mouseState);
	g_signal_connect(da, "enter-notify-event", G_CALLBACK(doFocus),	(gpointer) &mouseState);
	g_signal_connect(da, "leave-notify-event", G_CALLBACK(doFocus),	(gpointer) &mouseState);
	gtk_widget_set_events(da, gtk_widget_get_events(da)
								| GDK_LEAVE_NOTIFY_MASK
								| GDK_ENTER_NOTIFY_MASK
								| GDK_POINTER_MOTION_MASK
								| GDK_POINTER_MOTION_HINT_MASK
								| GDK_BUTTON_PRESS_MASK
								| GDK_BUTTON_RELEASE_MASK);
	
	gtk_window_set_position(GTK_WINDOW(topWindow), GTK_WIN_POS_CENTER);
	gtk_widget_show_all(topWindow);
	gtk_window_resize(GTK_WINDOW(topWindow), 300, 350);
}

int main(int argc, char **argv)
{
	int status = 0;
	GtkApplication *app;
	
	colorDir = BYW;		// default w/ no args
	if (argc > 1)
	{
		colorDir = strcmp(argv[1], "W")
					? BYH
					: BYW;
	}
//	colorDir = BYH;

 	app = gtk_application_new("dsample.example", G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
	status = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);

	return status;
}
