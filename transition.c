#include "CDSample.h"

extern cairo_surface_t *CTransition;
extern int transFlag[TTLTRANSITIONS];

#define TTLFADEFRAMES 25
#define ALPHAMAX .6			// going to grey = diff not interesting for alpha > ~.6

static gboolean transExec[TTLTRANSITIONS];		// executing state

// array of transition functions:
static void (*transFuncs[TTLTRANSITIONS])
	(GtkWidget *,			// widget being drawn to 
	cairo_surface_t *, 		// surface to draw from
	cairo_surface_t *, 		// surface to draw to
	int);					// transition length, in m-secs

typedef struct
{
	GtkWidget *da;				// drawing area widget
	cairo_surface_t *toSurf;	// cairo surface to transition to
} T_ARGS;

static void cleanUP(int transNo, T_ARGS *tArgs)
{
	// setting transFlag[] to 0 => doTransition() no longer called in DA callback 
	transFlag[transNo] = 0;
	transExec[transNo] = FALSE;
	free(tArgs);
	cairo_surface_destroy(CTransition);
	CTransition = NULL;
}

static gboolean _transFadeEXEC(T_ARGS *tArgs)
{
	gboolean ret = TRUE;		// TRUE = call us again, FALSE = we're done
	static int count = 0;
	static double alphaInc = ALPHAMAX / (double) TTLFADEFRAMES;
	double alpha;
	GtkWidget *da = tArgs->da;
	cairo_surface_t *to = tArgs->toSurf;
	
	cairo_t *cr = cairo_create(CTransition);
	
	alpha = alphaInc * (double) count;
	cairo_set_source_surface(cr, to, 0, 0);
	cairo_paint_with_alpha(cr, alpha);
	cairo_destroy(cr);

	if (count == TTLFADEFRAMES)
	{	
		cleanUP(TRANS_FADE, tArgs);
		count = 0;		// clunky, i know...		
		ret = FALSE;
	}
	
	count++;
	gtk_widget_queue_draw(da);
	return ret;
}

static void _transFadeINIT(GtkWidget *da, cairo_surface_t *from, cairo_surface_t *to, int mSecs)
{
	T_ARGS *tArgs = calloc(1, sizeof(T_ARGS));
	tArgs->da = da;
	tArgs->toSurf = to;
	
	transExec[TRANS_FADE] = TRUE;
	my_cairo_surface_copy(&CTransition, from, da);		// initialize
	g_timeout_add_full(G_PRIORITY_HIGH_IDLE, mSecs / TTLFADEFRAMES, 
						(GSourceFunc) _transFadeEXEC, tArgs, NULL);
}

static void initTFuncs()
{
	transFuncs[TRANS_FADE] = _transFadeINIT;
}

void doTransition(GtkWidget *da, cairo_surface_t *fromSurf, cairo_surface_t *toSurf)
{
	// by being called, we either need to:
	//   1) execute the requested transition effect
	//   2) simply return because the request is being processed
	
	// pointer to the transition function to execute
	void	(*transFunc)(GtkWidget *, 
						cairo_surface_t *,
						cairo_surface_t *,
						int);

	if (!transFuncs[0])
		initTFuncs();

	int transNo;
	for (transNo = 0; transNo < TTLTRANSITIONS; transNo++)
	{
		if (transExec[transNo])
		{	// we're already busy transitioning...
			break;
		}
		if (transFlag[transNo])
		{
			transFunc = transFuncs[transNo];
			(void)(*transFunc)(da, fromSurf, toSurf, transFlag[transNo]);
		}
	}

	return;
}

